# Getting started

# Requirements

1. Install nodejs
1. Install parcel
```bash
# If not already installed
$ npm install -g parcel-bundler@1.12.3
```

# Development

```bash
# Start development
$ parcel index.html
```

# References

- https://parceljs.org/
- https://nodejs.org/en/
